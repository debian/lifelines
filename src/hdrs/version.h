#ifndef _VERSION_H
#define _VERSION_H

/*
	This is isolated for convenience in marking
	a private build version. But this should be
	changed to default to the variable from configure.in.
*/

#define LIFELINES_VERSION "3.0.61"

#endif /* _VERSION_H */
